# importing libraries

import socket
from os import path
from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config
from screen import ScreenSettings
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from design import camera_
from webbrowser import open as webbrowser_open
from data.database import Database

# setting up screen

screen = ScreenSettings()
screen_width = screen.width() // 9 * 2
screen_height = screen.height() // 6 * 2
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', str(screen_width))
Config.set('graphics', 'height', str(screen_height))
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

# getting location of main folder

file_path_list = path.abspath(__file__)
backslash, file_path = "\ ", ""
file_path_list = file_path_list.split(backslash[0])
for n in file_path_list[0: len(file_path) - 2]:
    file_path += (n + backslash[0])

# creating camera database

default_camera_id = 'Camera 1'
default_camera_name = 'Main camera'

if not path.exists(file_path + '/graphic/data/st-camera.db'):
    database = Database('st-camera')
    database.new_table('camera', [('type', 'text', 'PRIMARY KEY'), ('information', 'text', '')])
    database.add_data('camera', ('Camera id', default_camera_id))
    database.add_data('camera', ('Camera link', ''))
    database.add_data('camera', ('Camera name', default_camera_name))
else:
    database = Database('st-camera')

database_information = ["", "", ""]

# creating connection to main thread

try:
    sock = socket.socket()
    sock.connect(('localhost', 5001))
except:
    pass

Builder.load_string(camera_(camera_id = database_information[0], camera_link = database_information[1], camera_name = database_information[2], height_ = screen_height))

class CameraSettings_Screen(Screen):
    def __init__(self, **kwargs):
        super(CameraSettings_Screen, self).__init__(**kwargs)
        data = database.read_table('camera')
        if data != []:
            self.ids['camera_id'].text = data[0][1]
            self.ids['camera_link'].text = data[1][1]
            self.ids['camera_name'].text = data[2][1]

    def exit_screen(self):
        exit()

    def help(self):
        webbrowser_open('https://github.com/jamesfrienkins3452/MCT')

    def update_data(self):
        # reading input data

        self.camera_id = self.ids['camera_id'].text
        self.camera_link = self.ids['camera_link'].text
        self.camera_name = self.ids['camera_name'].text

        # writing received data

        database.edit_data('camera',  ('type', self.camera_id), 'type', 'Camera id')
        database.edit_data('camera',  ('type', self.camera_link), 'type', 'Camera link')
        database.edit_data('camera',  ('type', self.camera_name), 'type', 'Camera name')

        # sending data

        data = self.camera_id + '|' + self.camera_link + '|' + self.camera_name
        sock.send(str.encode(data))

    def get_data(self):
        return database.read_table()


class SettingsApp(App):
    def build(self):
        screenManager = ScreenManager(transition = NoTransition())
        screenManager.add_widget(CameraSettings_Screen(name = 'camera_settings'))
        return screenManager

SettingsApp().run()