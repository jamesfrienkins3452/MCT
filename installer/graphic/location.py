# importing libraries

import socket
from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config
from screen import ScreenSettings
from os import path, getenv, getlogin
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from design import locations_
from webbrowser import open as webbrowser_open
from data.database import Database

# setting up screen

screen = ScreenSettings()
screen_width = screen.width() // 2
screen_height = screen.height() // 6 * 2
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', str(screen_width))
Config.set('graphics', 'height', str(screen_height))
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

# getting location of main folder

file_path_list = path.abspath(__file__)
backslash, file_path = "\ ", ""
file_path_list = file_path_list.split(backslash[0])
for n in file_path_list[0: len(file_path) - 2]:
    file_path += (n + backslash[0])

# creating camera database

default_settings_location = getenv('APPDATA')[0:len(getenv('APPDATA')) - 8] + '\\Local'
default_data_location = f'C:\\Users\\{getlogin()}\\Documents'
default_logs_location = f'C:\\Users\\{getlogin()}\\Documents'

if not path.exists(file_path + '/graphic/data/st-location.db'):
    database = Database('st-location')
    database.new_table('location', [('type', 'text', 'PRIMARY KEY'), ('location', 'text', '')])
    database.add_data('location', ('Program location', default_settings_location))
    database.add_data('location', ('Files location', default_data_location))
    database.add_data('location', ('Log location', default_logs_location))
else:
    database = Database('st-location')
    data = database.read_table('location')
    if data == None:
        database.new_table('location', [('type', 'text', 'PRIMARY KEY'), ('location', 'text', '')])
        database.add_data('location', ('Program location', default_settings_location))
        database.add_data('location', ('Files location', default_data_location))
        database.add_data('location', ('Log location', default_logs_location))

info = ["" , "", ""]

# creating connection to main thread

try:
    sock = socket.socket()
    sock.connect(('localhost', 5002))
except:
    pass

Builder.load_string(locations_(program_location = info[0], files_location = info[1], logs_location = info[2], height_ = screen_height))

class LocationsSettings_Screen(Screen):
    def __init__(self, **kwargs):
        super(LocationsSettings_Screen, self).__init__(**kwargs)
        if info[0] != "":
            self.ids['program_location_label'].color = (1, 1, 1, 0)
        if info[1] != "":
            self.ids['files_location_label'].color = (1, 1, 1, 0)
        if info[2] != "":
            self.ids['logs_location_label'].color = (1, 1, 1, 0)
        data = database.read_table('location')
        self.ids['program_location'].text = data[0][1]
        self.ids['files_location'].text = data[1][1]
        self.ids['logs_location'].text = data[2][1]

    def exit_screen(self):
        exit()

    def help(self):
        webbrowser_open('https://github.com/jamesfrienkins3452/MCT')

    def update_data(self):
        self.program_location = self.ids['program_location'].text
        self.files_location = self.ids['files_location'].text
        self.logs_location = self.ids['logs_location'].text

        if path.isdir(self.program_location) == False:
            self.program_location = ""
            self.ids['program_location_label'].color = (1, 1, 1, 1)
            database.edit_data('location',  ('type', default_settings_location), 'type', 'Program location')
        else:
            self.ids['program_location_label'].color = (1, 1, 1, 0)
            database.edit_data('location',  ('type', self.program_location), 'type', 'Program location')


        if path.isdir(self.files_location) == False:
            self.files_location = ""
            self.ids['files_location_label'].color = (1, 1, 1, 1)
            database.edit_data('location',  ('type', default_data_location), 'type', 'Files location')
        else:
            self.ids['files_location_label'].color = (1, 1, 1, 0)
            database.edit_data('location',  ('type', self.files_location), 'type', 'Files location')


        if path.isdir(self.logs_location) == False:
            self.logs_location = ""
            self.ids['logs_location_label'].color = (1, 1, 1, 1)
            database.edit_data('location',  ('type', default_logs_location), 'type', 'Log location')
        else:
            self.ids['logs_location_label'].color = (1, 1, 1, 0)
            database.edit_data('location',  ('type', self.logs_location), 'type', 'Log location')

        dt = self.program_location + '|' + self.files_location + '|' + self.logs_location
        sock.send(str.encode(dt))

    def get_data(self):
        return database.read_table()


class SettingsApp(App):
    def build(self):
        screenManager = ScreenManager(transition = NoTransition())
        screenManager.add_widget(LocationsSettings_Screen(name = 'locationssettings_screen'))
        return screenManager

SettingsApp().run()