# importing libraries

import os
import shutil
from graphic.data.database import Database

settings_location = os.getenv('APPDATA')[0:len(os.getenv('APPDATA')) - 8] + '\\Local'
default_program_location = os.getenv('APPDATA')[0:len(os.getenv('APPDATA')) - 8] + '\\Local'
default_data_location = f'C:\\Users\\{os.getlogin()}\\Documents'
default_logs_location = f'C:\\Users\\{os.getlogin()}\\Documents'

def remove_folder(folder):
    if os.path.isdir(folder):
        shutil.rmtree(folder)

def setup(key = "uninstall"):
    try:
        if os.path.exists(settings_location + '\\MCT\\installation settings.db'):
            database_installation_settings = Database('installation settings',  file_path = settings_location + '\\MCT')
            data = database_installation_settings.read_table('location')
            database_installation_settings.disconnect()
            for location in data:
                    remove_folder(location[1] + '\\MCT')
            remove_folder(settings_location + '\\MCT')
        else:
            remove_folder(default_program_location + '\\MCT')
            if key == "install":
                remove_folder(default_logs_location + '\\MCT')
            remove_folder(default_data_location + '\\MCT')
            remove_folder(settings_location + '\\MCT')
        key = "successfully"
    except:
        key = "unsuccessfully"
    return key