# importing libraries

import os
import socket
from requests import get as get_file
from graphic.data.database import Database

settings_location = os.getenv('APPDATA')[0:len(os.getenv('APPDATA')) - 8] + '\\Local'
IP = socket.gethostbyname(socket.gethostname())

def create_folder(folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)

default_program_location = os.getenv('APPDATA')[0:len(os.getenv('APPDATA')) - 8] + '\\Local'
default_data_location = f'C:\\Users\\{os.getlogin()}\\Documents'
default_logs_location = f'C:\\Users\\{os.getlogin()}\\Documents'
default_raw_urls_link = 'https://raw.githubusercontent.com/jamesfrienkins3452/MCT/main/raw-urls.txt'

def read_default_urls():
    raw = get_file(default_raw_urls_link, allow_redirects = True)
    file_urls = open(settings_location + '\\MCT\\raw_urls.txt', 'wb')
    file_urls.write(raw.content)
    file_urls.close()
    file_urls = open(settings_location + '\\MCT\\raw_urls.txt', 'r')
    data = file_urls.readlines()
    file_urls.close()

    links_for_download = []

    for index in range(len(data)):
        data[index] = data[index].replace('\n', '')

        item = data[index].split(' ')

        if index != 0:
            links_for_download.append([item[0], item[1], item[2]])

    return links_for_download

def download_program(prg_loc, dt_loc):
    raw_link_list = read_default_urls()

    print(raw_link_list)

    for data in raw_link_list:
        raw_link, folder, file_name = data
        raw = get_file(raw_link, allow_redirects = True)

        if folder != '':
            create_folder(prg_loc + '\\MCT\\system files\\program\\' + folder)
            open(prg_loc + '\\MCT\\system files\\program\\' + folder + '\\' + file_name, 'wb').write(raw.content)
        else:
            create_folder(prg_loc + '\\MCT\\system files\\program')
            open(prg_loc + '\\MCT\\system files\\program\\' + file_name, 'wb').write(raw.content)

def setup(camera_settings, program_location = default_program_location, data_location = default_data_location, logs_location = default_logs_location):
    try:
        key = "successfully"
        create_folder(settings_location + '\\MCT')
        create_folder(program_location + '\\MCT')
        create_folder(program_location + '\\MCT\\system files')
        create_folder(program_location + '\\MCT\\system files\\data')
        create_folder(program_location + '\\MCT\\system files\\cache')
        create_folder(program_location + '\\MCT\\system files\\program')
        create_folder(data_location + '\\MCT')
        create_folder(data_location + '\\MCT\\recordings')
        create_folder(logs_location + '\\MCT')
        create_folder(logs_location + '\\MCT\\logs')

        camera_id, camera_link, camera_name = camera_settings

        database_installation_settings = Database('installation settings',  file_path = settings_location + '\\MCT')
        database_installation_settings.new_table('location', [('key', 'text', 'PRIMARY KEY'), ('value', 'text', '')])
        database_installation_settings.add_data('location', ('program location', program_location))
        database_installation_settings.add_data('location', ('data location', data_location))
        database_installation_settings.add_data('location', ('logs location', logs_location))
        
        database_installation_settings.disconnect()

        database_camera_settings = Database('camera settings', file_path = program_location + '\\MCT\\system files\\data')
        database_camera_settings.new_table('camera_list', [('key', 'text', 'PRIMARY KEY'), ('name', 'text', '')])
        database_camera_settings.add_data('camera_list', ('cam_0', 'camera_0'))
        database_camera_settings.add_data('camera_list', ('cam_1', 'camera_1'))

        database_camera_settings.new_table('camera_1', [('key', 'text', 'PRIMARY KEY'), ('value', 'text', '')])
        database_camera_settings.add_data('camera_1', ('camera id', camera_id))
        database_camera_settings.add_data('camera_1', ('camera link', camera_link))
        database_camera_settings.add_data('camera_1', ('camera name', camera_name))

        database_camera_settings.new_table('camera_0', [('key', 'text', 'PRIMARY KEY'), ('value', 'text', '')])
        database_camera_settings.add_data('camera_0', ('camera id', 'Camera 0'))
        database_camera_settings.add_data('camera_0', ('camera link', f'http://{IP}'))
        database_camera_settings.add_data('camera_0', ('camera name', 'Default camera'))

        database_camera_settings.disconnect()

        download_program(program_location, data_location)
    except:
        key = "unsuccessfully"
    return key