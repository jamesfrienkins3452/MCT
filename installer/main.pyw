# importing libraries


from kivy.logger import Logger
Logger.disabled = True

import socket
import threading
from os import path, getenv, getlogin, system
from webbrowser import open as webbrowser_open
from graphic.data.database import Database
from internet.check_connection import Connection_status
from internet.check_speed import Connection_speed
from graphic.design import main
from kivy.config import Config
from kivymd.app import MDApp
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from graphic.screen import ScreenSettings
from functions.install import setup as setup_i
from functions.repair import setup as setup_r
from functions.uninstall import setup as setup_u

# setting up screen

screen_width, screen_height = ScreenSettings().width() // 5 * 2, ScreenSettings().height() // 5 * 2
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', str(screen_width))
Config.set('graphics', 'height', str(screen_height))
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

# getting location of main folder

backslash, file_path = "\ ", ""
for n in path.abspath(__file__).split(backslash[0])[0: len(file_path) - 1]:
    file_path += (n + backslash[0])
default_folder_locations = [['', '', ''], [getenv('APPDATA')[0:len(getenv('APPDATA')) - 8] + '\\Local', f'C:\\Users\\{getlogin()}\\Documents', f'C:\\Users\\{getlogin()}\\Documents']]

# creating threads to connect to other windows

def connection_to_camera_settings():
    while True:
        socket_camera = socket.socket()
        socket_camera.bind(('', 5001))
        socket_camera.listen(1)
        connection_camera, _ = socket_camera.accept()

        while True:
            try:
                info_c = connection_camera.recv(1024).decode()
                if info_c == '':
                    break
                default_folder_locations[0] = info_c.split('|')
            except:
                pass

def connection_to_location_settings():
    while True:
        socket_location = socket.socket()
        socket_location.bind(('', 5002))
        socket_location.listen(1)
        connection_location, _ = socket_location.accept()

        while True:
            try:
                info_l = connection_location.recv(1024).decode()
                if info_l == '':
                    break
                default_folder_locations[1] = info_l.split('|')
            except:
                pass

connection_to_camera_settings_THREAD = threading.Thread(target = connection_to_camera_settings, daemon = True).start()
connection_to_location_settings_THREAD = threading.Thread(target = connection_to_location_settings, daemon = True).start()

def open_help(key = None):
    if key == None:
        webbrowser_open('https://github.com/jamesfrienkins3452/MCT')

def run_script(file):
    def run_script_(file):
        system(f'python {file}')
    run_script_THREAD = threading.Thread(target = run_script_, args = (file, ), daemon = True)
    run_script_THREAD.start()


class Setup_Screen(Screen):
    def __init__(self, **kwargs):
        super(Setup_Screen, self).__init__(**kwargs)
        self.path_to_settings = default_folder_locations[1][0] + '\\MCT\\installation settings.db'
        self.program_installed_key = path.isfile(self.path_to_settings)

    def install(self):

        if self.program_installed_key == False:
            self.manager.current = 'installation_screen'
        else:
            self.manager.get_screen("setup_screen").ids.Setup_Screen_Install.disabled = True
            self.manager.get_screen("setup_screen").ids.Setup_Screen_Install_Label.text = "Program is already installed"

    def repair(self):
        if self.program_installed_key == True:
            self.manager.current = 'repair_screen'
        else:
            self.manager.get_screen("setup_screen").ids.Setup_Screen_Repair.disabled = True
            self.manager.get_screen("setup_screen").ids.Setup_Screen_Repair_Label.text = "Program isn't installed"

    def uninstall(self):
        self.manager.current = 'uninstallation_screen'

    def help(self):
        open_help()


class Installation_Screen(Screen): # Cannot change button visibility due to another thread
    def __init__(self, **kwargs):
        super(Installation_Screen, self).__init__(**kwargs)

    def check_connectioni_status(self): # problem with changing design in kivy outside of main thread
        if Connection_status().get_connection_status() == True:
            cn_speed = round(Connection_speed().get_download_speed() / 1048576, 2)
            if self.manager.current == "checkingconnectioni_screen":
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: Okay ({cn_speed} mb/sec)"
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 37, screen_height- 80
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_Continue.disabled = False
        else:
            if self.manager.current == "checkingconnectioni_screen":
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: No conection"
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 20, screen_height- 80

    def install(self):
        threading.Thread(target = self.check_connectioni_status, daemon = True).start()
        self.manager.current = 'checkingconnectioni_screen'

    def back(self):
        self.manager.current = 'setup_screen'

    def help(self):
        open_help()


class CheckingConnectionI_Screen(Screen):
    def __init__(self, **kwargs):
        super(CheckingConnectionI_Screen, self).__init__(**kwargs)

    def continue_installation(self):
        self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: Unknown"
        self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 0, screen_height- 80
        self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_Continue.disabled = True
        self.manager.current = 'installationsettings_screen'

    def back(self):
        self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: Unknown"
        self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 0, screen_height- 80
        self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_Continue.disabled = True
        self.manager.current = 'installation_screen'

    def help(self):
        open_help()


class InstallationSettings_Screen(Screen):
    def __init__(self, **kwargs):
        super(InstallationSettings_Screen, self).__init__(**kwargs)
        database = Database('', 'graphic\\data\\st-location.db' , key = True)
        data = database.read_table('location')

        if data != None:
            default_folder_locations[1][0] = data[0][1]
            default_folder_locations[1][1] = data[1][1]
            default_folder_locations[1][2] = data[2][1]
        else:
            pass

    def check_connectioni_status(self):
        if Connection_status().get_connection_status() == True:
            cn_speed = round(Connection_speed().get_download_speed() / 1048576, 2)
            if self.manager.current == "checkingconnectioni_screen":
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: Okay ({cn_speed} mb/sec)"
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 37, screen_height- 80
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_Continue.disabled = False
        else:
            if self.manager.current == "checkingconnectioni_screen":
                self.manager.get_screen("checkingconnectioni_screen").ids.Che1ckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: No conection"
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 20, screen_height- 80

    def update_labels(self):
        database = Database('', 'graphic\\data\\st-location.db' , key = True)
        data = database.read_table('location')

        if data != None:
            default_folder_locations[1][0] = data[0][1]
            default_folder_locations[1][1] = data[1][1]
            default_folder_locations[1][2] = data[2][1]
        else:
            pass

        if default_folder_locations[0][0] != '' and default_folder_locations[0][1] != '' and default_folder_locations[0][2] != '':
            self.manager.get_screen("installationsettings_screen").ids.camera_label.color = (1, 1, 1, 0)
            self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Continue.disabled = False
            self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Camera.disabled = True
            self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Location.disabled = True
        else:
            self.manager.get_screen("installationsettings_screen").ids.camera_label.color = (1, 1, 1, 1)
            self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Continue.disabled = True
            self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Camera.disabled = False
            self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Location.disabled = False

    def check_connection_status(self):        
        if Connection_status().get_connection_status() == True:
            cn_speed = round(Connection_speed().get_download_speed() / 1048576, 2)
            if self.manager.current == "checkingconnectioni_screen":
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: Okay ({cn_speed} mb/sec)"
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 37, screen_height- 80
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_Continue.disabled = False
        else:
            if self.manager.current == "checkingconnectioni_screen":
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.text = f"Connection status: No connection"
                self.manager.get_screen("checkingconnectioni_screen").ids.CheckingConnectionI_Screen_ConnectionStatus.pos = 20, screen_height- 80

    def continue_installation(self):
        self.manager.current = 'installationprocess_screen'

    def camera(self):
        run_script('graphic/camera.py')

    def location(self):
        run_script('graphic/location.py')

    def back(self):
        self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Camera.disabled = False
        self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Location.disabled = False
        self.manager.get_screen("installationsettings_screen").ids.camera_label.color = (1, 1, 1, 0)
        self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Continue.disabled = True
        self.manager.current = 'checkingconnectioni_screen'
        threading.Thread(target = self.check_connectioni_status, daemon = True).start()

    def help(self):
        open_help()


class InstallationProcess_Screen(Screen):
    def __init__(self, **kwargs):
        super(InstallationProcess_Screen, self).__init__(**kwargs)

    def install(self):
        uninstall_key = setup_u("install")
        if uninstall_key == "successfully":
            install_key = setup_i(default_folder_locations[0], program_location = default_folder_locations[1][0], data_location = default_folder_locations[1][1], logs_location = default_folder_locations[1][2])
            self.manager.get_screen("installationprocess_screen").ids.InstallationProccess_Screen_Back.disabled = True
            self.manager.get_screen("installationprocess_screen").ids.InstallationProcess_Screen_Install.disabled = True
            self.manager.get_screen("installationprocess_screen").ids.InstallationProcess_Screen_Continue.disabled = False
        if install_key == "successfully" and uninstall_key == "successfully":
            self.manager.get_screen("installationfinished_screen").ids.InstallationFinished_Screen_Label.size = (str(448), "0")
        else:
            self.manager.get_screen("installationfinished_screen").ids.InstallationFinished_Screen_Label.size = (str(482), "0")
        self.manager.get_screen("installationfinished_screen").ids.InstallationFinished_Screen_Label.text = f"Installation finished {install_key}"

    def continue_installation(self):
        self.manager.current = 'installationfinished_screen'

    def back(self):
        self.manager.current = 'installationsettings_screen'
        self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Camera.disabled = False
        self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Location.disabled = False
        self.manager.get_screen("installationsettings_screen").ids.camera_label.color = (1, 1, 1, 0)
        self.manager.get_screen("installationsettings_screen").ids.InstallationSettings_Screen_Continue.disabled = True

    def help(self):
        open_help()


class InstallationFinished_Screen(Screen):
    def __init__(self, **kwargs):
        super(InstallationFinished_Screen, self).__init__(**kwargs)

    def finish(key):
        exit()

    def help(self):
        open_help()


class Repair_Screen(Screen): # # Cannot change button visibility due to another thread
    def __init__(self, **kwargs):
        super(Repair_Screen, self).__init__(**kwargs)

    def check_connectionr_status(self):
        if Connection_status().get_connection_status() == True:
            cn_speed_ = round(Connection_speed().get_download_speed() / 1048576, 2)
            if self.manager.current == "checkingconnectionr_screen":
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.text = f"Connection status: Okay ({cn_speed_} mb/sec)"
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.pos = 37, screen_height- 80
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_Continue.disabled = False
        else:
            if self.manager.current == "checkingconnectionr_screen":
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.text = f"Connection status: No connection"
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.pos = 20, screen_height- 80

    def repair(self):
        threading.Thread(target = self.check_connectionr_status, daemon = True).start()
        self.manager.current = 'checkingconnectionr_screen'

    def back(self):
        self.manager.current = 'setup_screen'

    def help(self):
        open_help()


class CheckingConnectionR_Screen(Screen):
    def __init__(self, **kwargs):
        super(CheckingConnectionR_Screen, self).__init__(**kwargs)

    def continue_repairing(self):
        self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.text = f"Connection status: Unknown"
        self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.pos = 0, screen_height- 80
        self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_Continue.disabled = True
        self.manager.current = 'repairproccess_screen'

    def back(self):
        self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.text = f"Connection status: Unknown"
        self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.pos = 0, screen_height- 80
        self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_Continue.disabled = True
        self.manager.current = 'repair_screen'
        self.manager.current = 'checkingconnectionr_screen'

    def help(self):
        open_help()


class RepairProccess_Screen(Screen):
    def __init__(self, **kwargs):
        super(RepairProccess_Screen, self).__init__(**kwargs)

    def check_connection_status(self):
        if Connection_status().get_connection_status() == True:
            cn_speed = round(Connection_speed().get_download_speed() / 1048576, 2)
            if self.manager.current == "checkingconnectionr_screen":
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.text = f"Connection status: Okay ({cn_speed} mb/sec)"
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.pos = 37, screen_height- 80
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_Continue.disabled = False
        else:
            if self.manager.current == "checkingconnectionr_screen":
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.text = f"Connection status: No connection"
                self.manager.get_screen("checkingconnectionr_screen").ids.CheckingConnectionR_Screen_ConnectionStatus.pos = 20, screen_height- 80
    
    def repair(self):
        repair_key = setup_r()
        self.manager.get_screen("repairproccess_screen").ids.RepairProccess_Screen_Back.disabled = True
        self.manager.get_screen("repairproccess_screen").ids.RepairProccess_Screen_Repair.disabled = True
        self.manager.get_screen("repairproccess_screen").ids.RepairProccess_Screen_Continue.disabled = False
        if repair_key == "successfully":
            self.manager.get_screen("repairproccessfinished_screen").ids.RepairProccessFinished_Screen_Label.size = ("0", "0")
        else:
            self.manager.get_screen("repairproccessfinished_screen").ids.RepairProccessFinished_Screen_Label.size = ("34", "0")
        self.manager.get_screen("repairproccessfinished_screen").ids.RepairProccessFinished_Screen_Label.text = f"Repair finished {repair_key}"

    def continue_reparing(self):
        self.manager.current = 'repairproccessfinished_screen'

    def back(self):
        threading.Thread(target = self.check_connection_status, daemon = True).start()

    def help(self):
        open_help()


class RepairProccessFinished_Screen(Screen):
    def __init__(self, **kwargs):
        super(RepairProccessFinished_Screen, self).__init__(**kwargs)

    def finish(key):
        exit()

    def help(self):
            open_help()


class Uninstallation_Screen(Screen):
    def __init__(self, **kwargs):
        super(Uninstallation_Screen, self).__init__(**kwargs)

    def uninstall(self):
        self.manager.current = 'uninstallationproccess_screen'

    def back(self):
        self.manager.current = 'setup_screen'

    def help(self):
        open_help()


class UninstallationProccess_Screen(Screen):
    def __init__(self, **kwargs):
        super(UninstallationProccess_Screen, self).__init__(**kwargs)

    def uninstall(self):
        uninstall_key = setup_u()
        self.manager.get_screen("uninstallationproccess_screen").ids.UninstallationProccess_Screen_Back.disabled = True
        self.manager.get_screen("uninstallationproccess_screen").ids.UninstallationProcess_Screen_Uninstall.disabled = True
        self.manager.get_screen("uninstallationproccess_screen").ids.UninstallationProcess_Screen_Continue.disabled = False
        if uninstall_key == "successfully":
            self.manager.get_screen("uninstallationprocessfinished_Screen").ids.UninstallationFinished_Screen_Label.size = (str(425), "0")
        else:
            self.manager.get_screen("uninstallationprocessfinished_Screen").ids.UninstallationFinished_Screen_Label.size = (str(445), "0")
        self.manager.get_screen("uninstallationprocessfinished_Screen").ids.UninstallationFinished_Screen_Label.text = f"Uninstall finished {uninstall_key}"

    def continue_uninstalling(self):
        self.manager.current = 'uninstallationprocessfinished_Screen'

    def back(self):
        self.manager.current = 'uninstallation_screen'

    def help(self):
        open_help()


class UninstallationProcessFinished_Screen(Screen):
    def __init__(self, **kwargs):
        super(UninstallationProcessFinished_Screen, self).__init__(**kwargs)

    def finish(key):
        exit()

    def help(self):
        open_help()


class CameraApp(MDApp):
    def build(self):
        Builder.load_string(main(screen_height, screen_width))

        screenManager = ScreenManager(transition = NoTransition())

        screenManager.add_widget(Setup_Screen(name = 'setup_screen'))
        screenManager.add_widget(Installation_Screen(name = 'installation_screen'))
        screenManager.add_widget(CheckingConnectionI_Screen(name = 'checkingconnectioni_screen'))
        screenManager.add_widget(InstallationSettings_Screen(name = 'installationsettings_screen'))
        screenManager.add_widget(InstallationProcess_Screen(name = 'installationprocess_screen'))
        screenManager.add_widget(InstallationFinished_Screen(name = 'installationfinished_screen'))

        screenManager.add_widget(Repair_Screen(name = 'repair_screen'))
        screenManager.add_widget(CheckingConnectionR_Screen(name = 'checkingconnectionr_screen'))
        screenManager.add_widget(RepairProccess_Screen(name = 'repairproccess_screen'))
        screenManager.add_widget(RepairProccessFinished_Screen(name = 'repairproccessfinished_screen'))

        screenManager.add_widget(UninstallationProccess_Screen(name = 'uninstallationproccess_screen'))
        screenManager.add_widget(Uninstallation_Screen(name = 'uninstallation_screen'))
        screenManager.add_widget(UninstallationProcessFinished_Screen(name = 'uninstallationprocessfinished_Screen'))

        return screenManager

CameraApp().run()

