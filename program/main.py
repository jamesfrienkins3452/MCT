# importing libraries

import datetime
import threading
from plyer import filechooser
from os import path, getenv, system
from webbrowser import open as webbrowser_open
from functions.graphic.design import main
from functions.graphic.screen import ScreenSettings
from functions.data.logger import Logger, initializate
from functions.data.database import Database
from kivy.metrics import dp
from kivymd.app import MDApp
from kivy.lang import Builder
from kivy.config import Config
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivymd.uix.menu import MDDropdownMenu

# getting location of main folder

settings_location = getenv('APPDATA')[0:len(getenv('APPDATA')) - 8] + '\\Local'
backslash, file_path = "\ ", ""
for n in path.abspath(__file__).split(backslash[0])[0: len(file_path) - 1]:
    file_path += (n + backslash[0])

# setting up screen

screen_width, screen_height = ScreenSettings().width() // 5 * 2, ScreenSettings().height() // 5 * 2
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', str(screen_width))
Config.set('graphics', 'height', str(screen_height))
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

def read_default_settings() -> list:
    database = Database('installation settings', settings_location + '\\MCT', k = True)
    data = database.read_table('location')
    database.disconnect()
    folders = []
    for item in data:
        folders.append(item[1])
    return folders

def open_help(key = '') -> str:
    webbrowser_open('https://github.com/jamesfrienkins3452/MCT' + key)
    return 'https://github.com/jamesfrienkins3452/MCT' + key

def run_script(file) -> str:
    def run_file_cmd(file) -> None:
        system(f'python {file}')

    script_thread = threading.Thread(target = run_file_cmd, args = (file, ), daemon = True)
    script_thread.start()
    return f'python {file}'


class Main_Screen(Screen):
    def __init__(self, **kwargs) -> None:
        super(Main_Screen, self).__init__(**kwargs)

        self.settings = {
        'program_location': read_default_settings()[0],
        'recordings_location': read_default_settings()[1],
        'logs_location': read_default_settings()[2],
        'selected_detection_model': 'Standart dataset',
        'selected_camera': 'Camera 0',
        'email_notification_key': True,
        'background_work_key': False,
        'advanced_detection_key': False,
        'enable_map_key': False,
        'enable_rules_key': True,
        'enable_camera_preview_key': True,
        'launch_on_startup_key': False,
        'rules_set': '',
        'email': ''
        }

        self.cameraSettingDB = Database('camera settings', self.settings.get('program_location') + "\\MCT\\system files\\data")

        self.camera_settings_database = self.cameraSettingDB.read_table('camera_list'),

        # settings up dropdown list for buttons

        detection_model_menu_items = [
            {
                "text": f"Standart dataset",
                "viewclass": "OneLineListItem",
                "on_release": lambda x=f"Standart dataset": self.__model_menu_callback__(x),
            },
            {
                "text": f"Extended dataset",
                "viewclass": "OneLineListItem",
                "on_release": lambda x=f"Extended dataset": self.__model_menu_callback__(x),
            }
        ]
        self.detection_model_menu = MDDropdownMenu(
            caller = self.ids.detection_model_button,
            items = detection_model_menu_items,
            width_mult = 3,
            max_height = dp(100),
        )
        camera_selection_menu_items = [
            {
                "text": f"Camera 0",
                "viewclass": "OneLineListItem",
                "on_release": lambda x=f"Camera 0": self.__camera_menu_callback__(x),
            },
            {
                "text": f"Camera 1",
                "viewclass": "OneLineListItem",
                "on_release": lambda x=f"Camera 1": self.__camera_menu_callback__(x),
            }
        ]
        self.camera_selection_menu = MDDropdownMenu(
            caller = self.ids.camera_selection_button,
            items = camera_selection_menu_items,
            width_mult = 3,
            max_height = dp(100),
        )

        # initializating logger

        initializate(str(datetime.datetime.now())[0:19].replace(':', '-'), True)

    @Logger.logger_function
    def open_folder(self, key) -> str:
        if key == 'system':
            system('''explorer "''' + self.settings.get('program_location') + '''\\MCT\\system files"''')
            return {'key':'OPEN SYSTEM FOLDER'}
        elif key == 'logs':
            system("explorer " + self.settings.get('logs_location') + "\\MCT\\logs")
            return {'key':'OPEN LOGS FOLDER'}
        elif key == 'recordings':
            system("explorer " + self.settings.get('recordings_location') + "\\MCT\\recordi2ngs")
            return {'key':'OPEN RECORDS FOLDER'}
        elif key == 'models':
            system('''explorer "''' + self.settings.get('program_location') + '''\\MCT\\system files"''')
            return {'key':'OPEN MODELS FOLDER'}

    @Logger.logger_function
    def analyze_camera(self) -> None:
        return {'key':'ANALYZE CAMERA'}


    @Logger.logger_function
    def open_camera(self) -> None:
        return {'key': 'OPEN CAMERA'}


    @Logger.logger_function
    def add_camera(self) -> None:
        return {'key':'ADD CAMERA'}


    @Logger.logger_function
    def remove_camera(self) -> None:
        return {'key': 'REMOVE CAMERA'}


    @Logger.logger_function
    def screenshot(self) -> None:
        return {'key':'SCREENSHOT'}


    @Logger.logger_function
    def update_settings(self, *args) -> None:
        self.settings[args[0]] = args[1]

        if args[0] == 'enable_camera_preview_key':
            if self.settings.get(args[0]):
                self.manager.get_screen("setup_screen").ids.camera_preview_image.opacity = 1
            else:
                self.manager.get_screen("setup_screen").ids.camera_preview_image.opacity = 0
        elif args[0] == 'enable_map_key':
            pass
        elif args[0] == 'enable_rules_key':
            self.manager.get_screen("setup_screen").ids.rules_list.disabled = not args[1]
        elif args[0] == 'email_notification_key':
            self.manager.get_screen("setup_screen").ids.email_text_field.disabled = not args[1]
        return {'key':'UPDATE SETTINGS'}


    @Logger.logger_function
    def help(self) -> None:
        open_help()
        return {'key':'OPEN HELP'}


    def exit(self) -> None:
        exit()

    @Logger.logger_function
    def __model_menu_callback__(self, text_item) -> None:
        self.ids.detection_model_button.text = text_item
        self.settings['selected_detection_model'] = text_item
        return {'key':'SELECT MODEL'}


    @Logger.logger_function
    def __camera_menu_callback__(self, text_item) -> None:
        self.ids.camera_selection_button.text = text_item
        self.settings['selected_camera'] = text_item
        return {'key':'SELECT CAMERA'}


class MCTApp(MDApp):
    def build(self) -> ScreenManager:
        Builder.load_string(main())

        screenManager = ScreenManager(transition = NoTransition())
        screenManager.add_widget(Main_Screen(name = 'setup_screen'))

        return screenManager

MCTApp().run()
