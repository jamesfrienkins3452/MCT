a, b = '{', '}'

def main() -> str:
    main_screen = f"""
<Main_Screen>:
    MDBottomNavigation:
        panel_color: .3, .3, .3, 1
        selected_color_background: "orange"
        text_color_active: "lightgrey"
        MDBottomNavigationItem:
            name: 'screen 1'
            text: 'Main page'
            icon: 'account-box'
            FloatLayout:
                FitImage:
                    source: 'functions/images/installation_background.jpg'
                MDLabel:
                    text: "Welcome to MCT!"
                    pos_hint: {a}"right":1.05, "top":0.95{b}
                    size_hint: 1, 0.1
                    font_size: 40
                    color: "white"
                MDRaisedButton:
                    text: "Logs"
                    pos_hint: {a}"x":0.05, "y":0.55{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.open_folder('logs')
                MDRaisedButton:
                    text: "Recordings"
                    pos_hint: {a}"x":0.05, "y":0.45{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.open_folder('recordings')
                MDRaisedButton:
                    text: "System files"
                    pos_hint: {a}"x":0.05, "y":0.35{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.open_folder('system')
                MDRaisedButton:
                    text: "Help"
                    pos_hint: {a}"x":0.05, "y":0.05{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.help()
                MDRaisedButton:
                    text: "Exit"
                    pos_hint: {a}"right":0.95, "y":0.05{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.exit()
        MDBottomNavigationItem:
            name: 'screen 2'
            text: 'Camera list'
            icon: 'camera'
            FloatLayout:
                FitImage:
                    source: 'functions/images/installation_background.jpg'
                FitImage:
                    id: camera_preview_image
                    source: 'functions/images/installation_background.jpg'
                    size_hint: 0.58, 0.48
                    pos_hint: {a}"right":0.98, "top":0.7{b}
                MDRaisedButton:
                    id: camera_selection_button
                    text: "Camera 0"
                    pos_hint: {a}"x":0.4, "y":0.738{b}
                    # pos_hint: {a}"right":0.5, "top":0.95{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.camera_selection_menu.open()
                        # root.update_settings('enable_camera_preview_key', root.ids.enable_camera_preview_switch.active)
                MDLabel:
                    text: "Video settings"
                    pos_hint: {a}"right":1.05, "top":0.95{b}
                    size_hint: 1, 0.1
                    font_size: 40
                    color: "white"
                MDLabel:
                    text: "Camera preview"
                    pos_hint: {a}"x":0.055, "y":0.75{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDSwitch:
                    id: enable_camera_preview_switch
                    pos_hint: {a}"x":0.255, "y":0.712{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: True
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    on_active:
                        root.update_settings('enable_camera_preview_key', root.ids.enable_camera_preview_switch.active)
                MDLabel:
                    text: "Camera id:"
                    pos_hint: {a}"x":0.055, "y":0.62{b}
                    size_hint: 1, 0.1
                    color: "white"
                MDLabel:
                    text: "Camera link:"
                    pos_hint: {a}"x":0.055, "y":0.52{b}
                    size_hint: 1, 0.1
                    color: "white"
                MDLabel:
                    text: "Camera status:"
                    pos_hint: {a}"x":0.055, "y":0.42{b}
                    size_hint: 1, 0.1
                    color: "white"
                MDLabel:
                    text: "Time online:"
                    pos_hint: {a}"x":0.055, "y":0.32{b}
                    size_hint: 1, 0.1
                    color: "white"
                MDRaisedButton:
                    id: analyze_button
                    text: "Analyze"
                    pos_hint: {a}"x":0.4, "y":0.1{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.analyze_camera()
                MDRaisedButton:
                    id: open_button
                    text: "Open"
                    pos_hint: {a}"x":0.55, "y":0.1{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.open_camera()
                MDRaisedButton:
                    id: add_button
                    text: "Add"
                    pos_hint: {a}"x":0.7, "y":0.1{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.add_camera()
                MDRaisedButton:
                    id: remove_button
                    text: "Remove"
                    pos_hint: {a}"x":0.85, "y":0.1{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.remove_camera()
                MDRaisedButton:
                    id: screenshot_button
                    text: "Screenshot"
                    pos_hint: {a}"right":0.97, "y":0.738{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.screenshot()
        MDBottomNavigationItem:
            name: 'screen 3'
            text: 'Map'
            icon: 'map'
            FloatLayout:
                FitImage:
                    source: 'functions/images/installation_background.jpg'
                MDLabel:
                    text: "Map"
                    pos_hint: {a}"right":1.05, "top":0.95{b}
                    size_hint: 1, 0.1
                    font_size: 40
                    color: "white"
                MDLabel:
                    text: "Map enabled (beta)"
                    pos_hint: {a}"x":0.055, "y":0.75{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDSwitch:
                    id: enable_map_switch
                    pos_hint: {a}"x":0.255, "y":0.712{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: False
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    enable_camera_preview:
                    on_active:
                        root.update_settings('enable_map_key', root.ids.enable_map_switch.active)
        MDBottomNavigationItem:
            name: 'screen 4'
            text: 'Rules'
            icon: 'clipboard-list-outline'
            FloatLayout:
                FitImage:
                    source: 'functions/images/installation_background.jpg'
                MDLabel:
                    text: "Security rules"
                    pos_hint: {a}"right":1.05, "top":0.95{b}
                    size_hint: 1, 0.1
                    font_size: 40
                    color: "white"
                MDLabel:
                    text: "Enable rules"
                    pos_hint: {a}"x":0.055, "y":0.75{b}
                    size_hint: 0.2, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDSwitch:
                    id: enable_rules_switch
                    pos_hint: {a}"x":0.255, "y":0.712{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: True
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    on_active:
                        root.update_settings('enable_rules_key', root.ids.enable_rules_switch.active)
                MDTextField:
                    id: rules_list
                    mode: "fill"
                    hint_text: "Rules set"
                    helper_text_mode: "on_focus"
                    pos_hint: {a}"x":0.05, "y":0.1{b}
                    size_hint: 0.4, 0.6
                    multiline: True
                    on_text:
                        root.update_settings('rules_set', root.ids.rules_list.text)
                MDTextField:
                    id: rules_objects_list
                    mode: "fill"
                    text: "None"
                    hint_text: "Objects list"
                    helper_text_mode: "on_focus"
                    pos_hint: {a}"x":0.5, "y":0.1{b}
                    size_hint: 0.4, 0.6
                    multiline: True
                    disabled: True
        MDBottomNavigationItem:
            name: 'screen 5'
            text: 'Settings'
            icon: 'cog'
            FloatLayout:
                FitImage:
                    source: 'functions/images/installation_background.jpg'
                MDLabel:
                    text: "Settings"
                    pos_hint: {a}"right":1.05, "top":0.95{b}
                    size_hint: 1, 0.1
                    font_size: 40
                    color: "white"
                MDLabel:
                    text: "Email notifications"
                    pos_hint: {a}"x":0.055, "y":0.75{b}
                    size_hint: 0.5, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDTextField:
                    id: email_text_field
                    hint_text: "Email"
                    helper_text_mode: "on_focus"
                    pos_hint: {a}"x":0.5, "y":0.72{b}
                    size_hint: 0.3, 0.13
                    hint_text_color_normal: "black"
                    hint_text_color_focus: "black"
                    text_color_normal: "black"
                    text_color_focus: "black"
                    line_color_normal: "black"
                    line_color_focus: "black"
                    on_text:
                        root.update_settings('email', root.ids.email_text_field.text)
                MDSwitch:
                    id: email_notification_switch
                    pos_hint: {a}"x":0.85, "y":0.713{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: True
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    on_active:
                        root.update_settings('email_notification_key', root.ids.email_notification_switch.active)
                MDLabel:
                    text: "Background work"
                    pos_hint: {a}"x":0.055, "y":0.65{b}
                    size_hint: 0.5, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDSwitch:
                    id: background_work_switch
                    pos_hint: {a}"x":0.85, "y":0.613{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: False
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    on_active:
                        root.update_settings('background_work_key', root.ids.background_work_switch.active)
                MDLabel:
                    text: "Launch on startup"
                    pos_hint: {a}"x":0.055, "y":0.55{b}
                    size_hint: 0.5, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDSwitch:
                    id: launch_on_startup_switch
                    pos_hint: {a}"x":0.85, "y":0.513{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: False
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    on_active:
                        root.update_settings('launch_on_startup_key', root.ids.launch_on_startup_switch.active)
        MDBottomNavigationItem:
            name: 'screen 6'
            text: 'Developing'
            icon: 'wrench-cog'
            enabled: False
            FloatLayout:
                FitImage:
                    source: 'functions/images/installation_background.jpg'
                MDLabel:
                    text: "Advanced settings"
                    pos_hint: {a}"right":1.05, "top":0.95{b}
                    size_hint: 1, 0.1
                    font_size: 40
                    color: "white"
                MDLabel:
                    text: "Enable advanced detection (beta)"
                    pos_hint: {a}"x":0.055, "y":0.75{b}
                    size_hint: 0.5, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDSwitch:
                    id: advanced_detection_switch
                    pos_hint: {a}"right":0.93, "top":0.811{b}
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    active: False
                    icon_active: "check"
                    icon_inactive: "close"
                    icon_active_color: "white"
                    icon_inactive_color: "grey"
                    track_color_active: "white"
                    track_color_inactive: "grey"
                    opacity: 0.8
                    width: dp(50)
                    on_active:
                        root.update_settings('advanced_detection_key', root.ids.advanced_detection_switch.active)
                MDLabel:
                    text: "Add detection pre-trained model"
                    pos_hint: {a}"x":0.055, "y":0.65{b}
                    size_hint: 0.4, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDRaisedButton:
                    text: "Open folder"
                    pos_hint: {a}"right":0.97, "top":0.711{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.open_folder('models')
                MDLabel:
                    text: "Current detection model"
                    pos_hint: {a}"x":0.055, "y":0.55{b}
                    size_hint: 0.4, 0.05
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: "white"
                MDRaisedButton:
                    id: detection_model_button
                    text: "Standart dataset"
                    pos_hint: {a}"right":0.97, "top":0.611{b}
                    size_hint: 0.12, 0.03
                    background_normal: ""
                    background_color: 0.9, 0.9, 1, .8
                    color: 0, 0, 0
                    on_release:
                        root.detection_model_menu.open()
"""

    return main_screen