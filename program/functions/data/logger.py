import datetime
from functions.data.database import Database

def read_default_settings() -> list:
    from os import getenv
    settings_location = getenv('APPDATA')[0:len(getenv('APPDATA')) - 8] + '\\Local'
    database = Database('installation settings', settings_location + '\\MCT', k = True)
    data = database.read_table('location')
    database.disconnect()
    folders = []
    for item in data:
        folders.append(item[1])
    return folders

def initializate(key, __output__ = False) -> None:
    global log_key, output, database_logs, counter, table_name

    log_key, output = key, __output__
    program_location, recordings_location, logs_location = read_default_settings()
    table_name = (log_key.replace(' ', '-')).replace('-', '')

    database_logs = Database(f"session {log_key}", logs_location + f"\\MCT\\logs")
    database_logs.new_table(f"session{table_name}", [('key', 'text', 'PRIMARY KEY'), ('type', 'text', ''), ('time', 'text', ''), ('function', 'text', ''), ('arguments', 'text', '')])
    counter = 0

class Logger:
    def logger_function(input_function):
        def __logger_function__(*args, **kwargs) -> None:
            global counter, table_name, database_logs, log_key, output

            counter_key = '0' * (3 - len(str(counter))) + str(counter)
            current_time = str(datetime.datetime.now())[0:19]
            function_name = input_function.__name__
            dictionary = input_function(*args, **kwargs)
            log_text = f"\033[1;32m[{dictionary.get('key')}]\033[1;m " + ' '*(22-len(dictionary.get('key'))) + f"[{current_time}]   \033[1;93mfunction:\033[00m {function_name}" + ' '*(25-len(function_name)) + f" \033[1;93marguments:\033[00m {args}"
            database_logs.add_data(f'session{table_name}', (counter_key, dictionary.get('key'), current_time, function_name, str(args)))
            counter += 1

            if output == True:
                print(log_text)

        return __logger_function__